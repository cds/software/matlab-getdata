function [varargout] = nds2AddJava(varargin)
% nds2AddJava - Add Java classes for NDS2 client 
% [foundNds2,nds2Path] = nds2AddJava(dbgPrint)
%    dbgPrint - [OPTIONAL] if true - print where nds2.jar is found
%    foundNds2 - [OPTIONAL] - true/false if path to nds2.jar in javaclasspath
%    nds2Path - [OPTIONAL] if true, path to nds2.jar, otherwise empty
%
%  2024-05-22 - adapt to nds2.jar already in javaclasspath
%  2024-05-28 - only add path printout if requested
%  2024-06-12 - correctly prioritize using nds2.jar already in javaclasspath

% set flag for printout
dbgPrint = false;
if (nargin > 0)
    if (varargin{1} | vararging{1} == 'true')
        dbgPrint = true;
    end
end
foundNds2=false;
nds2Path = "";
% first check if it is in the javaclasspath
nds2Jar='nds2.jar';
curPath = javaclasspath('-all');
rowcol = size(curPath);
inPath = false;
for i=1:rowcol(1)
  for j=1:rowcol(2)
    tstStr = curPath{i,j};
    subPos = strfind(tstStr,nds2Jar);
    if ~isempty(subPos)
      inPath = true;
      nds2Path = tstStr; 
      if dbgPrint
        fprintf('nds2AddJava - found in javaclasspath: %s \n',nds2Path);
      end
      break
    end
  end
end
if inPath
  foundNds2 = true;
end
if (~foundNds2)
% if not in the javaclasspath, check for NDS2_JAR_PATH
  jarPath = getenv('NDS2_JAR_PATH');
  if jarPath ~= ""
    nds2Path = strtrim(jarPath);
    if dbgPrint
      fprintf('nds2AddJava - found defined NDS2_JAR_PATH: %s \n',nds2Path);
    end
  else
% next, try nds-client-config function
    [cmdStat,pathStr]=unix('nds-client-config --javaclasspath');
    nds2Path = strtrim(pathStr);
    if dbgPrint
      fprintf('nds2AddJava - found from nds-client-config: %s \n',nds2Path);
    end
  end
% if candidate javaclass path is not empty, add it
  if nds2Path ~= ""
    foundNds2 = true;
    javaaddpath(nds2Path);
    if dbgPrint
      fprintf('nds2AddJava: adding to javaclasspath\n');
    end
  end
end
if(~foundNds2)
  fprintf('nds2AddJava: ERROR - did not find nds2.jar in javaclasspath\n');
end
if nargout > 0
  varargout{1} = foundNds2;
  if nargout > 1
    varargout{2} = nds2Path;
  end
end
return
