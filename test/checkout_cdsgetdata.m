function [varargout] = checkout_cdsgetdata(varargin)
%([errCode]) = CHECKOUT_CDSGETDATA() - verify that cdsgetdata install is working
%  
%    errCode = [OPTIONAL] error code - 0 if pass
%
%  verifies that trend data is retrievable
%  verifies different input options

% clear error code
errCode = 0;
% Make sure cdsgetdata is in the MATLAB path
cdsgetdataPath = which('cdsgetdata');
if isempty(cdsgetdataPath)
    fprintf('checkout_cdsgetdata: cdsgetdata.m is not in MATLABPATH - FAIL\n');
    errCode = errCode + 1;
    if (nargout > 0)
        varargout{1}=errCode;
    end
    return    
end
fprintf('checkout_cdsgetdata: cdsgetdata.m is in MATLABPATH - PASS\n');
% Make sure LIGONDSIP is defined 
localNds=getenv('LIGONDSIP');
if isempty(localNds)
    fprintf('checkout_cdsgetdata: LIGONDSIP not defined - FAIL\n');
    errCode = errCode + 2;    
    if (nargout > 0)
        varargout{1} = errCode;
    end
    return    
end
fprintf('checkout_cdsgetdata: LIGONDSIP value of %s - PASS\n',localNds);
% Make sure LIGONDSIP is reachable
pingCmd = sprintf('ping -c 3 %s',localNds);
[pingStat,pingResp]=unix(pingCmd);
if pingStat > 0
    fprintf('checkout_cdsgetdata: local NDS1 server %s is not pingable - FAIL\n',localNds);
    errCode=errCode + 4;
    if (nargout > 0)
        varargout{1} = errCode;
    end
    return    
end
fprintf('checkout_cdsgetdata: local NDS1 server is pingable - PASS\n')
% Make sure NDS server is running
telnetCmd=sprintf('sleep 2 | telnet %s 8087',localNds);
[telnetStat,telnetResp]=unix(telnetCmd);
if ~contains(telnetResp,'daqd')
    fprintf('checkout_cdsgetdata: daqd not running on %s - FAIL\n',localNds);
    errCode=errCode+8
    if (nargout > 0)
        varargout{1} = errCode;
    end
    return    
end
fprintf('checkout_cdsgetdata: daqd is running on %s - PASS\n',localNds);
% Make sure nds2 class in in the java path
dbgPrint = true;
[foundNds,ndsPath]=nds2AddJava(dbgPrint);
if ~foundNds
    fprintf('checkout_cdsgetdata: Failed to find nds2 class - FAIL\n');
    errCode = errCode + 16;
    if (nargout > 0)
        varargout{1} = errCode;
    end
    return    
end
fprintf('checkout_cdsgetdata: nds2 in Java class path - PASS\n');

% try to make connection to local NDS1 server
localIp=8088;
try
    localConn=nds2.connection(localNds,localIp);
catch ME_nds
    fprintf('checkout_cdsgetdata: nds2 connection to %s %d failed %s - FAIL\n',localNds,localIp,ME_nds.message);
    errCode = errCode + 32;
    if (nargout > 0)
        varargout{1} = errCode;
    end
    return
end
fprintf('checkout_cdsgetdata: nds2 connection to %s %d made - PASS\n',localNds,localIp);
% If successful, get a count
try
    numChan=localConn.countChannels();
catch ME_nds
    fprintf('checkout_cdsgetdata: unable to get channel count from local NDS server %s - FAIL\n',ME_nds.message);
    errCode = errCode + 64;
    if (nargout > 0)
        varargout{1} = errCode;
    end
    return
end
fprintf('checkout_cdsgetdata: found %d channels on local NDS server - PASS\n',numChan);
% get a small set of channels and try to retrieve recent data
% create channel list with faster channels
try
    fastList=localConn.findChannels('*',nds2.channel.CHANNEL_TYPE_ONLINE,nds2.channel.DATA_TYPE_FLOAT32, 1024, 16384); 
catch ME_nds
    fprintf('checkout_cdsgetdata: Unable to retrieve list of fast channels from local NDS server %s - FAIL\n',ME_nds.message);
    errCode = errCode + 128;
    if (nargout > 0)
        varargout{1} = errCode;
    end
    return
end
fprintf('checkout_cdsgetdata: Got list of fast channels over 1 KHz from local NDS server - PASS\n');
% make sure we have some
numFast=length(fastList);
if (numFast < 3)     
    fprintf('checkout_cdsgetdata: Only %d fast channels being recorded on local NDS server - FAIL\n',numFast);
    errCode = errCode + 256;
    if (nargout > 0)
        varargout{1} = errCode;
    end
    return
end
fprintf('checkout_cdsgetdata: There are %d channels over 1 KHz from local NDS server - PASS\n',numFast);
% close this connection
localConn.close();
% Make sure gpstime is available
gpsCmd='gpstime -g';
[gpsStat,gpsResp]=unix(gpsCmd);
if gpsStat > 0
    fprintf('checkout_cdsgetdata: gpstime not in path - FAIL\n');
    errCode = errCode + 512;
    if (nargout > 0)
        varargout{1}=errCode;    
    end
    return    
end
fprintf('checkout_cdsgetdata: gpstime utility installed - PASS\n');
% truncate GPS time and set to 15 minutes in the past
gpsNow=round(str2num(gpsResp));
gpsStart = gpsNow - 15*60;
% Create channel list
chan{1,1}=char(fastList(1).name);
chan{1,2}=char(fastList(2).name);
chan{1,3}=char(fastList(3).name);
chan{1,4}=char(fastList(4).name);
chanNames={chan{1,1},chan{1,2},chan{1,3},chan{1,4}};
% call cdsgetdata
[chanListData,warnMsg]=cdsgetdata(chanNames,'raw',gpsStart,20);
if (~isempty(warnMsg))
    fprintf('checkout_cdsgetdata: warning on raw data\n %s',warnMsg);
end    
% make sure all is good
getOk = true;
for i=1:3
    if (chanListData(i).exist == 0)
        fprintf('checkout_cdsgetdata: Channel %i does not exist on NDS server \n',i);
        getOk = false;
    else
        if (~isempty(chanListData(i).error))
            fprintf('checkout_cdsgetdata: Channel %i has error %s\n',i,chanListData(i).error);
            getOk = false;
        end
    end
end
if getOk == false
    errCode = errCode + 1024;
    if (nargout > 0)
        varargout{1}=errCode;    
        fprintf('checkout_cdsgetdata: Unable to retrieve channel data on local NDS with cdsgetdata - FAIL\n');
    end
    return
end
fprintf('checkout_cdsgetdata: Success in retrieving raw channel data on local NDS with cdsgetdata - PASS\n');
%
% Now we try to get second, minute trend data, validating input options
%
%  second trends with no channel name mod - should get mean, correct rate
%  second trend - max,min,mean,rms
%  minute trends with no channel name mod - should get mean, correct rate
%  minute trend - max,min,mean,rms
%  second trend - max,min,mean same channel - check range

% start 4 hours back
trStart = gpsNow - 4*3600;
% try to get second trends - defaults to mean
[secListData,warnMsg]=cdsgetdata(chanNames,'second',trStart,2000);
if (~isempty(warnMsg))
    fprintf('checkout_cdsgetdata: warning on second trends\n %s',warnMsg);
end    
secOk = true;
secmean = ".mean,s-trend";
for i=1:4
    if (secListData(i).exist == 0)
        fprintf('checkout_cdsgetdata: Channel %i second trend not found \n',i)
        secOk = false;
    else
        if (~isempty(secListData(i).error))
            fprintf('checkout_cdsgetdata: Channel %i second trend has error %s\n',i,secListData(i).error);
            secOk = false;
        else
            if (~endsWith(secListData(i).name,secmean))
                fprintf('checkout_cdsgetdata: Channel %i trend name end is not %s\n',i,secmean);
                secOk = false;
            end
        end
    end
end
if secOk == true
    fprintf('checkout_cdsgetdata: Success in retrieving second trend data with cdsgetdata - PASS\n');
end

% Check for minute trends
[minListData,warnMsg]=cdsgetdata(chanNames,'minute',trStart,2400);
if (~isempty(warnMsg))
    fprintf('checkout_cdsgetdata: warning on minute trends\n %s',warnMsg);
end
minOk = true;
minmean = ".mean,m-trend";
for i=1:4
    if (minListData(i).exist == 0)
        fprintf('checkout_cdsgetdata: Channel %i minute trend not found \n',i)
        minOk = false;       
        if (~isempty(minListData(i).error))
            fprintf('checkout_cdsgetdata: Channel %i minute trend has error %s\n',i,minListData(i).error);
            minOk = false;
        else
            if (~endsWith(secListData(i).name,minmean))
                fprintf('checkout_cdsgetdata: Channel %i trend name end is not %s\n',i,minmean);
                minOk = false;
            end
        end
    end
end
if minOk == true
    fprintf('checkout_cdsgetdata: Success in retrieving minute trend data with cdsgetdata - PASS\n');
end
% if trends found, try to get min,max,mean,rms
tr(1,1)=".max";
tr(1,2)=".min";
tr(1,3)=".mean";
tr(1,4)=".rms";
chtr=cell(1,4);
for i=1:4
    chtr{1,i} = append(chan{1,i},tr(1,i));
end
trNames={char(chtr{1,1}),char(chtr{1,2}),char(chtr{1,3}),char(chtr{1,4})};
if secOk == true
    [secListData,warnMsg]=cdsgetdata(trNames,'STREND',trStart,2000);
    if (~isempty(warnMsg))
        fprintf('checkout_cdsgetdata: warning on second trends\n %s',warnMsg);
    end
    sectr=",s-trend";    
    for i=1:4
        if (secListData(i).exist == 0)
            fprintf('checkout_cdsgetdata: Channel %s not found \n',chtr{1,i})
            secOk = false;
        else
            if (~isempty(secListData(i).error))
                fprintf('checkout_cdsgetdata: Channel %s has error %s\n',chtr{1,i},secListData(i).error);
                secOk = false;
            else
                endstr = append(tr(1,i),sectr);
                if (~endsWith(secListData(i).name,endstr))
                    fprintf('checkout_cdsgetdata: Channel %i trend name end is not %s\n',i,endtr);
                    secOk = false;
                end
            end
        end
    end
    if secOk == true
        fprintf('checkout_cdsgetdata: Success in second min,max,mean,rms trend data with cdsgetdata - PASS\n');
    end
end
if minOk == true
    [minListData,warnMsg]=cdsgetdata(trNames,'MTREND',trStart,2400);
    if (~isempty(warnMsg))
        fprintf('checkout_cdsgetdata: warning on minute trends\n %s',warnMsg);
    end
    mintr=",m-trend";    
    for i=1:4
        if (minListData(i).exist == 0)
            fprintf('checkout_cdsgetdata: Channel %s not found \n',chtr{1,i})
            minOk = false;
        else
            if (~isempty(minListData(i).error))
                fprintf('checkout_cdsgetdata: Channel %s has error %s\n',chtr{1,i},minListData(i).error);
                minOk = false;
            else
                endstr = append(tr(1,i),mintr);
                if (~endsWith(minListData(i).name,endstr))
                    fprintf('checkout_cdsgetdata: Channel %i trend name end is not %s\n',i,endtr);
                    minOk = false;
                end
            end
        end
    end
    if minOk == true
        fprintf('checkout_cdsgetdata: Success in minute min,max,mean,rms trend data with cdsgetdata - PASS\n');
    end
end

% If we are on L1 or H1 system, we need to test the proxy where it shifts
% to remote NDS2 server if looking too far back
%  skip this test if not on L1 or H1 CDS

% Get local IFO from local channel names - skip out if not L1 or H1
ch1str = char(chan{1,1});
ifoPre = ch1str(1:2);
if ( ~strcmp(ifoPre,'L1') & ~strcmp(ifoPre,'H1') )
    fprintf('checkout_cdsgetdata: installed on non-IFO system - checkout complete\n');    
    if(nargout > 0)
        varargout{1} = 0;
    end
    return
end 
% first make sure we can see the needed NDS2 server
switch ifoPre
    case 'L1'
        remoteNds = 'nds.ligo-la.caltech.edu';
        ifoGlob = 'L1*';
        lookbackfile = strcat('/opt/rtcds/llo/l1/target/',localNds,'/logs/lookbacktimes.log');
    case 'H1'
        remoteNds = 'nds.ligo-wa.calech.edu';
        ifoGlob = 'H1*';
        lookbackfile = strcat('/opt/rtcds/lho/h1/target/',localNds,'/logs/lookbacktimes.log');
    otherwise
end
% Make sure remote NDS2 server is reachable
pingCmd = sprintf('ping -c 3 %s',remoteNds);
[pingStat,pingResp]=unix(pingCmd);
if pingStat > 0
    fprintf('checkout_cdsgetdata: NDS2 server %s is not pingable - FAIL\n',remoteNds);
    errCode=errCode + 2048;
    if (nargout > 0)
        varargout{1} = errCode;
    end
    return
end
% check for Kerberos ticket
klistCmd = 'klist -s';
[klistStat]=unix(klistCmd);
if klistStat > 0
    fprintf('checkout_cdsgetdata: No valid Kerberos ticket - FAIL if not on site workstation\n');
else
    fprintf('checkout_cdsgetdata: Valid Kerberos ticket - PASS\n');    
end
% try to make connection to remote NDS2 server
remoteIp=31200;
try
    remoteConn=nds2.connection(remoteNds,remoteIp);
catch ME_nds
    fprintf('checkout_cdsgetdata: nds2 connection to %s %d failed %s - FAIL\n',remoteNds,remoteIp,ME_nds.message);
    errCode = errCode + 4096;
    if (nargout > 0)
        varargout{1} = errCode;
    end
    return
end
fprintf('checkout_cdsgetdata: nds2 connection to %s %d made - PASS\n',remoteNds,remoteIp);
% If successful, get a count of IFO channels
chanType = nds2.channel.CHANNEL_TYPE_RAW;
try   
    numChan=remoteConn.countChannels(ifoGlob,chanType);
catch ME_nds
    fprintf('checkout_cdsgetdata: unable to get IFO channel count from NDS2 server %s - FAIL\n',ME_nds.message);
    errCode = errCode + 8192;
    if (nargout > 0)
        varargout{1} = errCode;
    end
    return
end
fprintf('checkout_cdsgetdata: found %d IFO channels on NDS2 server - PASS\n',numChan);
% close connection
remoteConn(close);
% make sure lookback data is in place for IFO
[fullGps,secGps,minGps]=check_framelookback(lookbackfile);
if isempty(fullGps)
    fprintf('checkout_cdsgetdata: lookbackfile %s not usable - FAIL \n',lookbackfile);
    errCode = errCode + 16384;
    if (nargout > 0)
        varargout{1} = errCode;
    end
    return
end
fprintf('checkout_cdsgetdata: Local lockback time for full frames is %d\n',fullGps);
fprintf('checkout_cdsgetdata: Local lookback time for second trend frames is %d\n',secGps);
fprintf('checkout_cdsgetdata: local lookback time for minute trend frames is %d\n',minGps);
% retrieve data from far enough back to hit remote server
gpsOld = fullGps-24*60*60;
[oldListData,warnMsg]=cdsgetdata(chanNames,'raw',gpsOld,60);
% make sure all is good
getOk = true;
for i=1:3
    if (oldListData(i).exist == 0)
        fprintf('checkout_cdsgetdata: Channel %i does not exist on remote NDS server \n',i);
        getOk = false;
    end
    if (~isempty(oldListData(i).error))
        fprintf('checkout_cdsgetdata: Channel %i has error %s\n',i,oldListData(i).error);
        getOk = false;
    end
end
if getOk == false
    errCode = errCode + 32768;
    if (nargout > 0)
        varargout{1}=errCode;    
        fprintf('checkout_cdsgetdata: Unable to retrieve channel data on remote NDS with cdsgetdata - FAIL\n');
    end
    return
end
fprintf('checkout_cdsgetdata: Success in retrieving channel data on remote NDS with cdsgetdata - PASS\n');

function [fullGps,secGps,minGps]=check_framelookback(lookBackFile)
% CHECK_FRAMELOOKBACK - Get GPS time of oldest frames on disk
%
%[fullGps,secGps,minGps]=check_framelookback(lookBackFile)
%
%   lookbackFile - log file of lookback GPS times
%
%  fullGps - starting GPS of oldest full frame file on disk
%  secGps - starting GPS of oldest second trend frame on disk
%  minGps - starting GPS of oldest minute trend frame on disk

fullGps=[];
secGps=[];
mingps=[];
fid = fopen(lookBackFile);
if fid < 0
    fprintf('FRAMELOOKBACK: could not open file %s\n',lookBackFile);
    return
end

while ~feof(fid)
    textline = fgetl(fid);
    [word1,rem1]=strtok(textline,' ');
    [word2,rem2]=strtok(rem1,' ');
    if strcmp(word2,'start')
        [word3,rem3]=strtok(rem2,' ');
        [word4,rem4]=strtok(rem3,' ');
	    if strcmp(word3,'GPS')
	        switch word1
		        case 'full'
		            fullGps = str2num(word4);
		        case 'second'
		            secGps = str2num(word4);
		        case 'minute'
		            minGps = str2num(word4);
                otherwise
            end
        end
    end
end
fclose(fid);

return

