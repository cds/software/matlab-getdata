function get_data2_test
% GETDATA2_TEST - test get_data2 against LLO, LHO, CIT NDS2 servers
%  requires 
%       matlab nds2 client
%       gpstime package
%       linux() MATLAB function
%       get_data2() MATLAB function
%

global CONFIG;
% list of channels for L1
l1Chan={'L1:CAL-DELTAL_EXTERNAL_DQ','L1:SUS-BS_M1_DRIVEALIGN_L_OUT_DQ','L1:ISI-ETMX_ST2_ISO_X_IN1_DQ','L1:PSL-OSC_PD_AMP_DC_OUTPUT'};
% list of channels for H1
h1Chan={'H1:CAL-DELTAL_EXTERNAL_DQ','H1:SUS-BS_M1_DRIVEALIGN_L_OUT_DQ','H1:ISI-ETMX_ST2_ISO_X_IN1_DQ','H1:PSL-OSC_PD_AMP_DC_OUTPUT'};
% combined channels
allChan=[l1Chan,h1Chan];
% get current time
[stat,gpsNowStr]=linux('gpstime -g -f %d');
% go back a couple days
gpsNow=str2num(gpsNowStr);
daySec = 86400;
gps2day = gpsNow - daySec * 2;
gps2week = gpsNow - daySec * 14;
gps6week = gpsNow - daySec * 42;
durTest = 100;
%
% make sure Kerberos
[stat,kinfo]=linux('klist -f');

% test get_data2 as is

% Get L1 data - 2 days
fprintf(' Get L1 data - 2 days ago\n');
llodat=get_data2(l1Chan,'raw',gps2day,durTest);
% Get H1 data - 2 days
fprintf(' Get H1 data - 2 days ago\n');
lhodat=get_data2(h1Chan,'raw',gps2day,durTest);
% go L1,H1 data - 2 days
fprintf(' Get L1,H1 data - 2 days ago\n');
alldat=get_data2(allChan,'raw',gps2day,durTest);
% go back a couple weeks
fprintf(' Get L1 data - 2 weeks ago\n');
llodat=get_data2(l1Chan,'raw',gps2week,durTest);
fprintf(' Get H1 data - 2 weeks ago\n');
lhodat=get_data2(h1Chan,'raw',gps2week,durTest);
fprintf(' Get L1,H1 data - 2 weeks ago\n');
alldat=get_data2(allChan,'raw',gps2week,durTest);
% go back six weeks
fprintf(' Get L1 data - 6 weeks ago\n');
llodat=get_data2(l1Chan,'raw',gps6week,durTest);
fprintf(' Get H1 data - 6 weeks ago\n');
lhodat=get_data2(h1Chan,'raw',gps6week,durTest);
fprintf(' Get L1,H1 data - 6 weeks ago\n');
alldat=get_data2(allChan,'raw',gps6week,durTest);

return
