function [varargout] = checkout_get_data(varargin)
% ([errCode]) = CHECKOUT_GET_DATA - verify that get_data install is working
%
%    - also confirms all the input options for trend variables
%    errCode = [OPTIONAL] error code - 0 if pass

% Clear errors 
errCode = 0;
% Make sure get_data is in the MATLAB path
get_dataPath = which('get_data');
if isempty(get_dataPath)
    fprintf('checkout_get_data: get_data.m is not in MATLABPATH - FAIL\n');
    errCode = errCode + 1;
    if (nargout > 0)
        varargout{1}=errCode;
    end
    return    
end
fprintf('checkout_get_data: get_data.m is in MATLABPATH - PASS\n');
% Make sure LIGONDSIP is defined 
localNds=getenv('LIGONDSIP');
if isempty(localNds)
    fprintf('checkout_get_data: LIGONDSIP not defined - FAIL\n');
    errCode = errCode + 2;    
    if (nargout > 0)
        varargout{1} = errCode;
    end
    return    
end
fprintf('checkout_get_data: LIGONDSIP value of %s - PASS\n',localNds);
% Make sure LIGONDSIP is reachable
pingCmd = sprintf('ping -c 3 %s',localNds);
[pingStat,pingResp]=unix(pingCmd);
if pingStat > 0
    fprintf('checkout_get_data: NDS server %s is not pingable - FAIL\n',localNds);
    errCode=errCode + 4;
    if (nargout > 0)
        varargout{1} = errCode;
    end
    return    
end
fprintf('checkout_get_data: NDS server is pingable - PASS\n')
% Make sure NDS server is running
telnetCmd=sprintf('sleep 2 | telnet %s 8087',localNds);
[telnetStat,telnetResp]=unix(telnetCmd);
if ~contains(telnetResp,'daqd')
    fprintf('checkout_get_data: daqd not running on %s - FAIL\n',localNds);
    errCode=errCode+8
    if (nargout > 0)
        varargout{1} = errCode;
    end
    return    
end
fprintf('checkout_get_data: daqd is running on %s - PASS\n',localNds);
% Make sure nds2 class in in the java path
dbgPrint = true;
[foundNds,ndsPath]=nds2AddJava(dbgPrint);
if ~foundNds
    fprintf('checkout_get_data: Failed to find nds2 class - FAIL\n');
    errCode = errCode + 16;
    if (nargout > 0)
        varargout{1} = errCode;
    end
    return    
end
fprintf('checkout_get_data: nds2 in Java class path - PASS\n');
% try to make connections
localIp=8088;
try
    localConn=nds2.connection(localNds,localIp);
catch ME_nds
    fprintf('checkout_get_data: nds2 connection to %s %d failed %s - FAIL\n',localNds,localIp,ME_nds.message);
    errCode = errCode + 32;
    if (nargout > 0)
        varargout{1} = errCode;
    end
    return
end
fprintf('checkout_get_data: nds2 connection to %s %d made - PASS\n',localNds,localIp);
% If successful, get a count
try
    numChan=localConn.countChannels();
catch ME_nds
    fprintf('checkout_get_data: unable to get channel count from NDS server %s - FAIL\n',ME_nds.message);
    errCode = errCode + 64;
    if (nargout > 0)
        varargout{1} = errCode;
    end
    return
end
fprintf('checkout_get_data: found %d channels on NDS server - PASS\n',numChan);
% get a small set of channels and try to retrieve recent data
% create channel list with faster channels
try
    fastList=localConn.findChannels('*',nds2.channel.CHANNEL_TYPE_ONLINE,nds2.channel.DATA_TYPE_FLOAT32, 1024, 16384); 
catch ME_nds
    fprintf('checkout_get_data: Unable to retrieve list of fast channels from NDS server %s - FAIL\n',ME_nds.message);
    errCode = errCode + 128;
    if (nargout > 0)
        varargout{1} = errCode;
    end
    return
end
fprintf('checkout_get_data: Got list of fast channels over 1 KHz from NDS server - PASS\n');
% make sure we have some
numFast=length(fastList);
if (numFast < 4)     
    fprintf('checkout_get_data: Only %d fast channels being recorded on NDS server - FAIL\n',numFast);
    errCode = errCode + 256;
    if (nargout > 0)
        varargout{1} = errCode;
    end
    return
end
fprintf('checkout_get_data: There are %d channels over 1 KHz from NDS server - PASS\n',numFast);
% close this connection
localConn.close();
% Make sure gpstime is available
gpsCmd='gpstime -g';
[gpsStat,gpsResp]=unix(gpsCmd);
if gpsStat > 0
    fprintf('checkout_get_data: gpstime not in path - FAIL\n');
    errCode = errCode + 512;
    if (nargout > 0)
        varargout{1}=errCode;    
    end
    return    
end
fprintf('checkout_get_data: gpstime utility installed - PASS\n');
% truncate GPS time and set to 15 minutes in the past
gpsNow=round(str2num(gpsResp));
gpsStart = gpsNow - 15*60;
% Create channel list
chan{1,1}=char(fastList(1).name);
chan{1,2}=char(fastList(2).name);
chan{1,3}=char(fastList(3).name);
chan{1,4}=char(fastList(4).name);
chanNames={chan{1,1},chan{1,2},chan{1,3},chan{1,4}};
% call get_data
[chanListData,warnMsg]=get_data(chanNames,'raw',gpsStart,20);
if (~isempty(warnMsg))
    fprintf('checkout_get_data: warning on raw data\n %s \n',warnMsg);
end    
% make sure all is good
getOk = true;
for i=1:4
    if (chanListData(i).exist == 0)
        fprintf('checkout_get_data: Channel %i does not exist on NDS server \n',i);
        getOk = false;
    else
        if (~isempty(chanListData(i).error))
            fprintf('checkout_get_data: Channel %i has error %s\n',i,chanListData(i).error);
            getOk = false;
        end
    end        
end
if getOk == false
    errCode = errCode + 1024;
    if (nargout > 0)
        varargout{1}=errCode;    
        fprintf('checkout_get_data: Unable to retrieve raw channel data with get_data - FAIL\n');
    end
    return
end
fprintf('checkout_get_data: Success in retrieving raw channel data with get_data - PASS\n');
%
% Now we try to get second, minute trend data, validating input options
%
%  second trends with no channel name mod - should get mean, correct rate
%  second trend - max,min,mean,rms
%  minute trends with no channel name mod - should get mean, correct rate
%  minute trend - max,min,mean,rms
%  second trend - max,min,mean same channel - check range

% start 4 hours back
trStart = gpsNow - 4*3600;
% try to get second trends - defaults to mean
[secListData,warnMsg]=get_data(chanNames,'second',trStart,2000);
if (~isempty(warnMsg))
    fprintf('checkout_get_data: warning on second trends\n %s \n',warnMsg);
end    
secOk = true;
secmean = ".mean,s-trend";
for i=1:4
    if (secListData(i).exist == 0)
        fprintf('checkout_get_data: Channel %i second trend not found \n',i)
        secOk = false;
    else
        if (~isempty(secListData(i).error))
            fprintf('checkout_get_data: Channel %i second trend has error %s\n',i,secListData(i).error);
            secOk = false;
        else
            if (~endsWith(secListData(i).name,secmean))
                fprintf('checkout_get_data: Channel %i trend name end is not %s\n',i,secmean);
                secOk = false;
            end
        end
    end
end
if secOk == true
    fprintf('checkout_get_data: Success in retrieving second trend data with get_data - PASS\n');
end

% Check for minute trends
[minListData,warnMsg]=get_data(chanNames,'minute',trStart,2400);
if (~isempty(warnMsg))
    fprintf('checkout_get_data: warning on minute trends\n %s \n',warnMsg);
end
minOk = true;
minmean = ".mean,m-trend";
for i=1:4
    if (minListData(i).exist == 0)
        fprintf('checkout_get_data: Channel %i minute trend not found \n',i)
        minOk = false;       
        if (~isempty(minListData(i).error))
            fprintf('checkout_get_data: Channel %i minute trend has error %s\n',i,minListData(i).error);
            minOk = false;
        else
            if (~endsWith(secListData(i).name,minmean))
                fprintf('checkout_get_data: Channel %i trend name end is not %s\n',i,minmean);
                minOk = false;
            end
        end
    end
end
if minOk == true
    fprintf('checkout_get_data: Success in retrieving minute trend data with get_data - PASS\n');
end
% if trends found, try to get min,max,mean,rms
tr(1,1)=".max";
tr(1,2)=".min";
tr(1,3)=".mean";
tr(1,4)=".rms";
chtr=cell(1,4);
for i=1:4
    chtr{1,i} = append(chan{1,i},tr(1,i));
end
trNames={char(chtr{1,1}),char(chtr{1,2}),char(chtr{1,3}),char(chtr{1,4})};
if secOk == true
    [secListData,warnMsg]=get_data(trNames,'STREND',trStart,2000);
    if (~isempty(warnMsg))
        fprintf('checkout_get_data: warning on second trends\n %s \n',warnMsg);
    end
    sectr=",s-trend";    
    for i=1:4
        if (secListData(i).exist == 0)
            fprintf('checkout_get_data: Channel %s not found \n',chtr{1,i})
            secOk = false;
        else
            if (~isempty(secListData(i).error))
                fprintf('checkout_get_data: Channel %s has error %s\n',chtr{1,i},secListData(i).error);
                secOk = false;
            else
                endstr = append(tr(1,i),sectr);
                if (~endsWith(secListData(i).name,endstr))
                    fprintf('checkout_get_data: Channel %i trend name end is not %s\n',i,endtr);
                    secOk = false;
                end
            end
        end
    end
    if secOk == true
        fprintf('checkout_get_data: Success in second min,max,mean,rms trend data with get_data - PASS\n');
    end
end
if minOk == true
    [minListData,warnMsg]=get_data(trNames,'MTREND',trStart,2400);
    if (~isempty(warnMsg))
        fprintf('checkout_get_data: warning on minute trends\n %s \n',warnMsg);
    end
    mintr=",m-trend";    
    for i=1:4
        if (minListData(i).exist == 0)
            fprintf('checkout_get_data: Channel %s not found \n',chtr{1,i})
            minOk = false;
        else
            if (~isempty(minListData(i).error))
                fprintf('checkout_get_data: Channel %s has error %s\n',chtr{1,i},minListData(i).error);
                minOk = false;
            else
                endstr = append(tr(1,i),mintr);
                if (~endsWith(minListData(i).name,endstr))
                    fprintf('checkout_get_data: Channel %i trend name end is not %s\n',i,endtr);
                    minOk = false;
                end
            end
        end
    end
    if minOk == true
        fprintf('checkout_get_data: Success in minute min,max,mean,rms trend data with get_data - PASS\n');
    end
end
if(nargout > 0)
    varargout{1} = 0;
end
