function [status,result] = linux(unixCmd)
% LINUX - run linux commands without MATLAB directory paths
%     
% [s,w]=linux('command')
%
%  Wraps a call to normal MATLAB unix()
%  - strips LD_LIBRARY_PATH down, makes call, restores LD_LIBRARY_PATH

%  we remove anything in the LD_LIBRARY_PATH that has a 'matlab' directory
libEnvName = 'LD_LIBRARY_PATH';
mlPattern = 'matlab';

% Get the current LD_LIBRARY_PATH and split into chunks
mlLibPath = getenv(libEnvName);

mlLibDirs=regexp(mlLibPath, ':', 'split');
separator='';
cdsLibPath='';

% Loop over the directories, only keep those without matlab in them
for pathElem=mlLibDirs
    flattenedElem=pathElem{1,1};
    mlMatch=strfind(flattenedElem, mlPattern);
    if isempty(mlMatch)
	   cdsLibPath=strcat(cdsLibPath, separator, flattenedElem);
% after first path, change separator to semi-colon 
       separator=':';
    end
end

% temporarily set to clean path
setenv(libEnvName,cdsLibPath);
% call our function
try
   [status,result] = unix(unixCmd);
catch MExc
   fprintf(' ERROR in linux() for command %s\n',unixCmd);
   MExc
end
% set it back
setenv(libEnvName,mlLibPath);
return
end



