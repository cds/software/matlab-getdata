function [chanListData,warnMsg]=cdsgetdata(channels,varargin)
% CDSGETDATA - Get CDS channel data from NDS1/2 server
%
% form 1) chanListData = cdsgetdata(channels,start_time,duration[,nds2site])
%	channels: cell-array of channel names 
%        names are NDS2 format [chName(.trName)(,rate/type),(rate/type)]
%         chName - channel name [required]
%         trName - trend name if trend type [optional]
%         type - frame type (m-trend,s-trend) [optional]
%         rate - data rate (in Hz) [optional]
%	start_time: start time in GPS seconds
%	duration: amount of data in seconds
%   nds2site: [OPTIONAL] specify which nds2 server - 'cit','llo' or 'lho'
%
% form 2) chanListData = cdsgetdata(channels,dtype,start_time,duration[,nds2site])
%	channels - cell-array of channel names 
%        names are simple format [chName(.trName)]
%         chName - channel name [required]
%         trName - trend name if trend type [optional]
%       dtype: data type (e.g. 'raw','minute','MTREND','second','STREND')
%	start_time - start time in GPS seconds
%	duration - amount of data in seconds
%   nds2site: [OPTIONAL] specify which nds2 server - 'cit','llo' or 'lho'
%
% Will use local CDS NDS1 server if data available there, otherwise
%   a remote NDS2 depending on which IFOs (L1, H1, both, etc.) 
%   or what 'nds2site' is set to
%
% chanListData is a structure with data in it
%   .name - channel name
%   .data - time-series (double)
%   .rate - data rate (Hz)
%   .start - starting time (GPS seconds)
%   .duration - duration of time-series (seconds)
%   .exist - 0 if not found, 1 if found
%   .error - error on this channel
%
% -- to avoid printout of warning messages, add another output parameter
%   
%   [chanListData,warnMsg] = cdsgetdata(...)
%
%   2017-04-06  Keith Thorne
%   2020-05-08  Keith Thorne - add optional warnMsg output to stop 
%                              warning message preing
%   2021-01-06  Keith Thorne - correct handling of errors for single-channel queries
%   2021-01-11  Keith Thorne - add missing fclose() in framelookback function
%   2021-07-02  Keith Thorne - remove GAP_HANDLER for raw data, add 'nds2site' input
%   2023-03-01  Brian Lantz - line 331, re-added channel name to successful queries
%   2023-11-03  Edgard Bonilla - corrected looping of channel names of
%                                successful and unsuccessful queries
%   2023-12-14  Keith Thorne - add NDS2_JAR_PATH for alternate nds2.jar
%                            location
%   2024-02-08  Keith Thorne - add support for test-stands (non-H/L data)
%   2024-03-22  Keith Thorne - Handle *:GDS-, *:DMT- channels as NDS2 only
%   2024-05-22  Keith Thorne - use external nds2AddJava() in package

% clear variables
chanListData=[];
warnMsg=[];
% figure out input format
nds2site=[];
if (nargin == 5)
    dtype = varargin{1};
    start_time = varargin{2};
    duration = varargin{3};
    nds2site = varargin{4};
elseif (nargin == 3)
    dtype = [];
    start_time = varargin{1};
    duration = varargin{2};
elseif (nargin == 4)
    trytype = varargin{1};
    if ~isnumeric(trytype)
       dtype = varargin{1};
       start_time = varargin{2};
       duration = varargin{3}; 
    else
       dtype = [];
       start_time = varargin{1};
       duration = varargin{2};
       nds2site = varargin{3};
    end 
else
    fprintf('CDSGETDATA ERROR: number of inputs is wrong - should be 3 or 4\n');
    return
end
% check output count
warnPrt = true;
if (nargout > 1)
   warnPrt = false;
end

% Add nds2 Java to java class path
[foundNds2]=nds2AddJava();
if(foundNds2 == false)
    fprintf('CDSGETDATA ERROR: nds2.jar not in Java classpath\n');
    return
end

%  set local NDS
localNds = getenv('LIGONDSIP');
if isempty(localNds)
    warnMsg = 'WARNING: local NDS server not defined';
    if warnPrt
       fprintf('CDSGETDATA %s\n',warnMsg);
    end    
end
localIp = 8088;
% convert single-channel string to cell-array
if isstr(channels)
  channels = {channels};
end

% check that dtype, if used, is valid
rawData = false;
secTrend = false;
minTrend = false;
if ~isempty(dtype)
    switch dtype
	case 'raw'
	    rawData = true;           
	case 'RAW'
	    rawData = true;           
	case 'second'
	    secTrend = true;
	case 'strend'
	    secTrend = true;
	case 'STREND'
	    secTrend = true;
	case 'minute'
	    minTrend = true;
	case 'mtrend'
	    minTrend = true;
	case 'MTREND'
	    minTrend = true;
	otherwise
	    fprintf('CDSGETDATA ERROR: Frame datatype of %s is unknown\n',dtype);
    end
end
		
% Figure out the type of data
[chanRow,chanCol] = size(channels);
numChan=0;
numL=0;
numH=0;
numX=0;
numRaw=0;
numSec=0;
numMin=0;
numDMT=0;
for i=1:chanRow
  for j=1:chanCol
    thisChan=channels{i,j};
    numChan = numChan + 1;
    [chName,trName,sub1,sub2] = splitnds2chan(thisChan);   
    switch chName(1:1)
        case 'L'
            numL = numL + 1;
        case 'H'
            numH = numH + 1;
        otherwise
            numX = numX + 1;
    end
% also check for non-CDS channels from DMT (*:GDS, *:DMT)
    switch chName(4:6)
	case 'DMT'
	    numDMT = numDMT + 1;
	case 'GDS'
	    numDMT = numDMT + 1;
        otherwise
    end
% If needed, figure out type of channel
    if ~isempty(dtype)
        sub2 = [];
        if secTrend
	    sub1 = 's-trend';
	elseif minTrend
	    sub1 = 'm-trend';
        else
	    sub1 = [];
	end
    else
        rawData = false;
        secTrend = false;
        minTrend = false;
        if ~isempty(sub1)
          switch sub1
            case 's-trend'
              secTrend = true;
              sub1 = 's-trend';
            case 'strend'
              secTrend = true;
              sub1 = 's-trend';
	    case 'STREND'
              secTrend = true;
              sub1 = 's-trend';
	    case 'second'
              secTrend = true;
              sub1 = 's-trend';
            case 'm-trend'
              minTrend = true;
              sub1 = 'm-trend';
            case 'mtrend'
              minTrend = true;
              sub1 = 'm-trend';
            case 'MTREND'
              minTrend = true;
              sub1 = 'm-trend';
            case 'minute'
              minTrend = true;
              sub1 = 'm-trend';
            otherwise
              if isempty (sub2)
                rawData = true;
              else
                switch sub2
                  case 's-trend'
                    secTrend = true;
                    sub2 = 's-trend';
                  case 'strend'
                    secTrend = true;
                    sub2 = 's-trend';
                  case 'STREND'
                    secTrend = true;
                    sub2 = 's-trend';
                  case 'second'
                    secTrend = true;
                    sub2 = 's-trend';
                  case 'm-trend'
                    minTrend = true;
                    sub2 = 'm-trend';
                  case 'mtrend'
                    minTrend = true;
                    sub2 = 'm-trend';
                  case 'MTREND'
                    minTrend = true;
                    sub2 = 'm-trend';
                  case 'minute'
                    minTrend = true;
                    sub2 = 'm-trend';
                  otherwise
                    rawData = true;    
                end
              end  
          end
        else
          rawData = true;
        end
    end
% if this is a trend channel and no trend name, add it
    if rawData
           numRaw = numRaw + 1;
    elseif secTrend 
           numSec = numSec + 1;
           if isempty(trName)
               trName = 'mean';
           end
    elseif minTrend
           numMin = numMin + 1;
           if isempty(trName)
               trName = 'mean';
           end
    else
           fprintf('CDSGETDATA ERROR: channel spec %s unclear - assume raw\n');
           numRaw = numRaw + 1;
    end
 % re-assemble channel
    thisChan=chName;
    if ~isempty(trName)
        thisChan=strcat(thisChan,'.',trName);
    end
    if ~isempty(sub1)
        thisChan=strcat(thisChan,',',sub1);
        if ~isempty(sub2)
            thisChan=strcat(thisChan,',',sub2);
        end
    end
% create list for call       
    chanList{i,j} = thisChan;
  end
end
% skip out if not channels
if (numChan==0)
    fprintf('CDSGETDATA ERROR:  empty channel list\n,');
    return
end
 % skip out if mixing IFO data and test stand data
if (numX > 0 & ((numL+numH) > 0))
    fprintf('CDSGETDATA ERROR: mixing L/H data and test stand data\n');
    return
end
% if NDS2 server parameter is set, use that for site
siteDef = false;
remoteSite = [];
remoteNds = [];
remoteIp = [];
if ~isempty(nds2site)
   siteDef = true;
end 
if (siteDef)
    fprintf(' Using input def of NDS2 site - %s\n',nds2site);
    nds2site = lower(nds2site);
    if (strcmp(nds2site,'lho'))
      remoteSite = 'lho';
      remoteNds = 'nds.ligo-wa.caltech.edu';
      remoteIp = 31200;
    elseif (strcmp(nds2site,'llo'))
      remoteSite = 'llo';
      remoteNds = 'nds.ligo-la.caltech.edu';
      remoteIp = 31200;
    elseif (strcmp(nds2site,'cit'))
      remoteSite = 'cit';
      remoteNds = 'nds.ligo.caltech.edu';
      remoteIp = 31200;
    else
      fprintf(' nds2site unknown - use cit\n');
      remoteSite = 'cit';
      remoteNds = 'nds.ligo.caltech.edu';
      remoteIp = 31200;
    end
% otherwise, set possible remote NDS based on channels
% - all channels start with L - LLO
% - all channels start with H - LHO
% - anything else - CIT
else
  if(numChan == numH)
    remoteSite = 'lho';
    remoteNds = 'nds.ligo-wa.caltech.edu';
    remoteIp = 31200;
  elseif (numChan == numL)
    remoteSite = 'llo';
    remoteNds = 'nds.ligo-la.caltech.edu';
    remoteIp = 31200;
  else
    remoteSite = 'cit';
    remoteNds = 'nds.ligo.caltech.edu';
    remoteIp = 31200;
  end
end
%truncate the start time to a GPS second
startTime = floor(abs(start_time));
% round duration up to nearest second
durTime = round(duration);
% calculate stop time
stopTime = startTime+durTime;
% if minute trend, set times to divisible by 60
if numMin > 0
   startTime = 60*round(startTime/60);
   stopTime = 60*round(stopTime/60);
   durTime = stopTime - startTime;        
end
% note if changing time range
if ((startTime ~= start_time) | (durTime ~= duration))
    warnMsg = sprintf('WARNING: start time, duration corrected to %d and %d\n',startTime,durTime);
    if warnPrt
       fprintf('CDSGETDATA %s\n',warnMsg);
    end    
end
%
%  now to decide - if we only have channels that match the local IFO, we
%  need to check the lookback time on the localNds.  If it is enough, use
%  that, otherwise, we go remote...
%  -- if the number of DMTs channels is non-zero, use NDS2
%
% If test stand, local is only way to go
if (numX > 0)
    useLocal = true;
else
    useLocal = false;
    tryLocal = false;
    if strcmp(localNds(1:2),'l1') && (numL == numChan) && (numDMT == 0)
        tryLocal = true;
        lookbackfile = strcat('/opt/rtcds/llo/l1/target/',localNds,'/logs/lookbacktimes.log');
    elseif strcmp(localNds(1:2),'h1') &&  (numH == numChan) && (numDMT == 0)
        tryLocal = true;
        lookbackfile = strcat('/opt/rtcds/lho/h1/target/',localNds,'/logs/lookbacktimes.log');
    end
    if tryLocal
        [fullGps,secGps,minGps]=framelookback(lookbackfile);
        if ((numRaw == 0) | (fullGps < startTime))
            if ((numSec == 0) | (secGps < startTime))
                useLocal = true;
            end
        end
    end
end
%
% remember which NDS1/2 server we are using.
% - We have to have a different persistent object for each possible one
% because java object don't play nice
persistent activeSite;
persistent localConn;
persistent localPrevClock;
persistent lloConn;
persistent lloPrevClock;
persistent lhoConn;
persistent lhoPrevClock;
persistent citConn;
persistent citPrevClock;

if useLocal
   newSite = 'local';
   ndsServer = localNds;
   ndsIp = localIp;
else
   newSite = remoteSite; 
   ndsServer = remoteNds;
   ndsIp = remoteIp;
end

% set time to renew things at
renewTime = 180;

if ~strcmp(newSite,activeSite)
    activeSite = newSite;
    switch activeSite
      case 'local'
        if isempty(localConn)    
          try
            localConn=nds2.connection(ndsServer,ndsIp);
          catch ME_nds
            fprintf('CDSGETDATA ERROR: nds connection to %s %d failed %s\n',ndsServer,ndsIp,ME_nds.message);
            return
          end
      % in CDS, we patch gaps on local data 
          localConn.setParameter('GAP_HANDLER','STATIC_HANDLER_ZERO');
          fprintf('CDSGETDATA: nds connection to %s %d succeeded\n',ndsServer,ndsIp);
       else
          nowClock = clock;
          elapseTime = etime(nowClock,localPrevClock);
          if elapseTime > renewTime
              localConn.close()
              try
                  localConn=nds2.connection(ndsServer,ndsIp);
              catch ME_nds
                  fprintf('CDSGETDATA ERROR: nds connection to %s %d failed %s\n',ndsServer,ndsIp,ME_nds.message);
                  return
              end
      % in CDS, we always patch gaps 
              localConn.setParameter('GAP_HANDLER','STATIC_HANDLER_ZERO');
              fprintf('CDSGETDATA: renew nds connection to %s %d succeeded\n',ndsServer,ndsIp);
          end
        end  
      case 'lho'
        if isempty(lhoConn)    
          try
            lhoConn=nds2.connection(ndsServer,ndsIp);
          catch ME_nds
            fprintf('CDSGETDATA ERROR: nds connection to %s %d failed %s\n',ndsServer,ndsIp,ME_nds.message);
            return
          end
    % in CDS, only fill gaps in minute trends in remote data
          if (numChan == numMin)
              lhoConn.setParameter('GAP_HANDLER','STATIC_HANDLER_ZERO');
          else
              lhoConn.setParameter('GAP_HANDLER','ABORT_HANDLER');
          end
    % We also always wait for the data
    	  lhoConn.setParameter('ALLOW_DATA_ON_TAPE','1');
          fprintf('CDSGETDATA: nds connection to %s %d succeeded\n',ndsServer,ndsIp);
        else
          nowClock = clock;
          elapseTime = etime(nowClock,lhoPrevClock);
          if elapseTime > renewTime
              lhoConn.close()
              try
                  lhoConn=nds2.connection(ndsServer,ndsIp);
              catch ME_nds
                  fprintf('CDSGETDATA ERROR: nds connection to %s %d failed %s\n',ndsServer,ndsIp,ME_nds.message);
                  return
              end
    % in CDS, only fill gaps in minute trends in remote data
              if (numChan == numMin)
                  lhoConn.setParameter('GAP_HANDLER','STATIC_HANDLER_ZERO');
              else
                  lhoConn.setParameter('GAP_HANDLER','ABORT_HANDLER');
              end
    % We also always wait for the data
    	      lhoConn.setParameter('ALLOW_DATA_ON_TAPE','1');
              fprintf('CDSGETDATA: renew nds connection to %s %d succeeded\n',ndsServer,ndsIp);
          end
        end
      case 'llo'
        if isempty(lloConn)    
          try
            lloConn=nds2.connection(ndsServer,ndsIp);
          catch ME_nds
            fprintf('CDSGETDATA ERROR: nds connection to %s %d failed %s\n',ndsServer,ndsIp,ME_nds.message);
            return
          end
    % in CDS, only fill gaps in minute trends in remote data
          if (numChan == numMin)
              lloConn.setParameter('GAP_HANDLER','STATIC_HANDLER_ZERO');
          else
              lloConn.setParameter('GAP_HANDLER','ABORT_HANDLER');
          end
    % We also always wait for the data
    	  lloConn.setParameter('ALLOW_DATA_ON_TAPE','1');
          fprintf('CDSGETDATA: nds connection to %s %d succeeded\n',ndsServer,ndsIp);
        else
          nowClock = clock;
          elapseTime = etime(nowClock,lloPrevClock);
          if elapseTime > renewTime
              lloConn.close()
              try
                  lloConn=nds2.connection(ndsServer,ndsIp);
              catch ME_nds
                  fprintf('CDSGETDATA ERROR: nds connection to %s %d failed %s\n',ndsServer,ndsIp,ME_nds.message);
                  return
              end
    % in CDS, only fill gaps in minute trends in remote data
              if (numChan == numMin)
                  lloConn.setParameter('GAP_HANDLER','STATIC_HANDLER_ZERO');
              else
                  lloConn.setParameter('GAP_HANDLER','ABORT_HANDLER');
              end
    % We also always wait for the data
    	      lloConn.setParameter('ALLOW_DATA_ON_TAPE','1');
              fprintf('CDSGETDATA: renew nds connection to %s %d succeeded\n',ndsServer,ndsIp);
          end
        end  
      case 'cit'
        if isempty(citConn)    
          try
            citConn=nds2.connection(ndsServer,ndsIp);
          catch ME_nds
            fprintf('CDSGETDATA ERROR: nds connection to %s %d failed %s\n',ndsServer,ndsIp,ME_nds.message);
            return
          end
    % in CDS, only fill gaps in minute trends in remote data
          if (numChan == numMin)
              citConn.setParameter('GAP_HANDLER','STATIC_HANDLER_ZERO');
          else
              citConn.setParameter('GAP_HANDLER','ABORT_HANDLER');
          end
    % We also always wait for the data
    	  citConn.setParameter('ALLOW_DATA_ON_TAPE','1');
          fprintf('CDSGETDATA: nds connection to %s %d succeeded\n',ndsServer,ndsIp);
        else
          nowClock = clock;
          elapseTime = etime(nowClock,citPrevClock);
          if elapseTime > renewTime
              citConn.close()
              try
                  citConn=nds2.connection(ndsServer,ndsIp);
              catch ME_nds
                  fprintf('CDSGETDATA ERROR: nds connection to %s %d failed %s\n',ndsServer,ndsIp,ME_nds.message);
                  return
              end
    % in CDS, only fill gaps in minute trends in remote data
              if (numChan == numMin)
                  citConn.setParameter('GAP_HANDLER','STATIC_HANDLER_ZERO');
              else
                  citConn.setParameter('GAP_HANDLER','ABORT_HANDLER');
              end
    % We also always wait for the data
    	      citConn.setParameter('ALLOW_DATA_ON_TAPE','1');
              fprintf('CDSGETDATA: renew nds connection to %s %d succeeded\n',ndsServer,ndsIp);
          end
        end
      otherwise
          fprintf('CDSGETDATA ERROR: unknown NDS site %s \n',activeSite);
    end  
end

% FIRST TRY: do it with all channels at once. If no errors, we are done.
% If we fail, do it over, one channel at a time, to detail the errors.
[chanRow,chanCol] = size(chanList);
idx=0;
chanGlob=cell(1);
numChan = 0;
for i=1:chanRow
    for j=1:chanCol
        idx = idx + 1;
        numChan = idx;
% build channel list for 'fetch'        
        chanGlob{1,idx} = chanList{i,j}; 
    end
end
% do full list fetch
try
    switch activeSite
	    case 'local'
            dataBuff = localConn.fetch(startTime,stopTime,chanGlob);
        case 'lho'
            dataBuff = lhoConn.fetch(startTime,stopTime,chanGlob);
        case 'llo'
            dataBuff = lloConn.fetch(startTime,stopTime,chanGlob);
        case 'cit'
            dataBuff = citConn.fetch(startTime,stopTime,chanGlob);
    end
    fetchOk = true;       
catch ME_fetch
    fetchOk = false; 
    rtIdx = strfind(ME_fetch.message,'RuntimeException');
      if(rtIdx > 0)
          nfIdx = strfind(ME_fetch.message, 'Requested data were not found');
          if (nfIdx > 0)
              errMsg = 'Channel not found';
          else
              errMsg = 'RunTimeException';
          end
      else
          errMsg = ME_fetch.message;
      end
      fprintf('CDSGETDATA ERROR: nds2 failed on fetch  %s\n',ME_fetch.message);  
end
if (fetchOk ) 
% now tediously loop over channels again to pull out data
  idx=0;
  for i=1:chanRow
    for j=1:chanCol
        idx = idx+1;
        thisChan = chanGlob{1,idx};
        chanData = dataBuff(idx).getData();
        if ~isempty(chanData)
            chanListData(idx).exist = 1;
            chanListData(idx).error = [];
        else
            chanListData(idx).exist = 0;
            chanListData(idx).error = errMsg;
        end   
        chanListData(idx).name = thisChan;
        chanListData(idx).data = double(chanData);            
        chanRate = dataBuff(idx).getChannel().getSampleRate();
        chanListData(idx).rate = chanRate;
        chanListData(idx).start = startTime;
        chanListData(idx).duration = durTime;
    end
  end
% if not OK but only one channel we are done
elseif (numChan == 1)
  thisChan = chanGlob{1,1};
  chanListData(1).name = thisChan;
  chanListData(1).data = [];
  chanListData(1).rate = [];
  chanListData(1).start = startTime;
  chanListData(1).duration = durTime;
  chanListData(1).exist = 0;
  chanListData(1).error = errMsg;
% otherwise, re-do per channel to get specific errors
else
  fprintf('CDSGETDATA: Redo fetch to get per-channel errors\n');
  idx=0;
  for i=1:chanRow
    for j=1:chanCol
        idx = idx + 1; 
        thisChan = chanGlob{1,idx};
        try
            switch activeSite
	            case 'local'
                    dataBuff = localConn.fetch(startTime,stopTime,thisChan);
                case 'lho'
                    dataBuff = lhoConn.fetch(startTime,stopTime,thisChan);
                case 'llo'
                    dataBuff = lloConn.fetch(startTime,stopTime,thisChan);
                case 'cit'
                    dataBuff = citConn.fetch(startTime,stopTime,thisChan);
            end
            chanData = dataBuff(1).getData();
            chanRate = dataBuff(1).getChannel().getSampleRate();
        catch ME_chan
            rtIdx = strfind(ME_chan.message,'RuntimeException');
            if(rtIdx > 0)
                nfIdx = strfind(ME_chan.message, 'Requested data were not found');
                if (nfIdx > 0)
                    errMsg = 'Channel not found';
                else
                    errMsg = 'RunTimeException';
                end
            else
                errMsg = ME_chan.message;
            end
            fprintf('CDSGETDATA ERROR: nds2 failed on %s - %s\n',thisChan,ME_chan.message);  
            chanData = [];
            chanRate = [];
        end   
        chanListData(idx).name = thisChan;
        chanListData(idx).data = double(chanData);
        chanListData(idx).rate = chanRate;
        chanListData(idx).start = startTime;
        chanListData(idx).duration = durTime;
        if ~isempty(chanData)
            chanListData(idx).exist = 1;
            chanListData(idx).error = [];
        else
            chanListData(idx).exist = 0;
            chanListData(idx).error = errMsg;
        end   
    end
  end
end  
% save clock
switch activeSite
    case 'local'
        localPrevClock = clock;
    case 'lho'
        lhoPrevClock = clock;
    case 'llo'
        lloPrevClock = clock;
    case 'cit'
        citPrevClock = clock;
end

return

function [fullGps,secGps,minGps]=framelookback(lookBackFile)
% FRAMELOOKBACK - Get GPS time of oldest frames on disk
%
%[fullGps,secGps,minGps]=framelookback(lookBackFile)
%
%   lookbackFile - log file of lookback GPS times
%
%  fullGps - starting GPS of oldest full frame file on disk
%  secGps - starting GPS of oldest second trend frame on disk
%  minGps - starting GPS of oldest minute trend frame on disk

fullGps=[];
secGps=[];
mingps=[];
fid = fopen(lookBackFile);
if fid < 0
    fprintf('CDSGETDATA ERROR: could not open file %s\n',lookBackFile);
    return
end

while ~feof(fid)
    textline = fgetl(fid);
    [word1,rem1]=strtok(textline,' ');
    [word2,rem2]=strtok(rem1,' ');
    if strcmp(word2,'start')
        [word3,rem3]=strtok(rem2,' ');
        [word4,rem4]=strtok(rem3,' ');
	    if strcmp(word3,'GPS')
	        switch word1
		        case 'full'
		            fullGps = str2num(word4);
		        case 'second'
		            secGps = str2num(word4);
		        case 'minute'
		            minGps = str2num(word4);
                otherwise
            end
        end
    end
end
fclose(fid);

return

function [chName,trName,sub1,sub2] = splitnds2chan(chantext)
% SPLITNDS2CHAN - split nds2 channel name into pieces
%
%  [chName,trName,sub1,sub2] = splitnds2chan(chantext)
%    chantext - text of channel specification in NDS2 format
%
%   chName - channel name (without possible trend modifier)
%   trName - trend channel modifier (after a period)
%   sub1 - sub-classifier (rate, frame type) after first comma
%   sub2 - sub-classifier (rate, frame type) after second comma

chName = [];
trName = [];
sub1 = [];
sub2 = [];
if exist('strsplit') == 5
    chanParts = strsplit(chantext,',');
    numParts = length(chanParts);
    fullname = cell2str(chanParts{1});
    if numParts > 1
        sub1 = cell2str(chanParts{2});
        if numParts > 2
            sub2 = cell2str(chanParts{3});
        end
    end
else
    [fullname,rem1] = strtok(chantext,',');
    if ~isempty(rem1)
        [sub1,rem2] = strtok(rem1(2:end),',');
        if ~isempty(rem2)
            sub2 = rem2(2:end);
        end
    end
end
% now see if trend suffix after period in name
[chName,trName] = strtok(fullname,'.');
if ~isempty(trName)
    trName = trName(2:end);
else
    trName = [];
end
return
