#!/bin/bash

set -e

mainbranch=main
version=$1

if [[ -z $version ]]
then
  echo "ERROR: a tagged dtt version must be passed on the command line"
  exit
fi

src_dir=$(pwd)
echo "Source directory is ${src_dr}"

# get version tag
version_tag=$(echo $version | sed 's/~/_/g')
echo "Releasing version ${version} from git tag '${version_tag}'"

# check if tag exists
git_tags=$(git tag)
found=false
for check_tag in $git_tags
do
  if [[ $check_tag == $version_tag ]]
  then
    found=true
    break
  fi
done
if ! $found
then
  echo "ERROR: Version tag not found.  Be sure to tag the version before releasing!"
  exit
fi

# check if tag is ancestor of main
if ! git merge-base --is-ancestor ${version_tag} ${mainbranch}
then
  echo "ERROR: version tag must be an ancestor of the ${mainbranch} branch"
  exit
fi

# check if tag already released
target_deb_tag="debian/${version_tag}-"
echo $target_deb_tag
for check_tag in $git_tags
do
  if [[ "${check_tag}" == *"${target_deb_tag}"* ]]
  then
    echo "ERROR: version already released in tag ${check_tag}"
    exit
  fi
done

# check out the tag
if ! git checkout ${version_tag}
then
  echo "Couldn't checkout tag '${version_tag}'"
  exit
fi

# make sure checkout is clean
num_changes=$(git diff-index HEAD | wc -l)
if [[ $num_changes -gt 0 ]]
then
  echo "ERROR: Source directory is not clean."
  git switch ${mainbranch}
  exit
fi

# Check actual version number in source to see if it's correct.  Don't release packages with wrong version number.
#setup_ver=$( cat setup.py | grep version | grep -Eo '[0-9]+\.[0-9]+\.[0-9]+' )
#init_ver=$( cat src/ligo_softioc/__init__.py | grep __version__ | grep -Eo '[0-9]+\.[0-9]+\.[0-9]+' )
#if [[ $setup_ver != $init_ver ]]
#then
#  echo "Error: version in setup.py, '${setup_ver}' doesn't equal '${init_ver}', the version in src/ligo_softioc/__init__.py"
#  git switch ${mainbranch}
#  exit
#fi
#if [[ $setup_ver != $version ]]
#then
#  echo "Error: Requested version was ${version} but source version at tag ${version_tag} was ${setup_ver}"
#  git switch ${mainbranch}
#  exit
#fi

git switch ${mainbranch}
echo "passed all checks"

# start the release
first_run=true
tmp_chng_entry='/tmp/_dtt_deb_changelog_entry.txt'
deb_chglog='debian/changelog'
tmp_chng_log='/tmp/_dtt_deb_changelog.txt'


git switch "debian"
git pull
gbp import-ref ${mainbranch} -u $version --upstream-tag='%(version)s' --debian-branch=debian

# manually edit first changelog
old_line_count=$(cat ${deb_chglog} | wc -l)
gbp dch -Rc -N ${version}-1+deb$deb_ver --debian-branch debian
new_line_count=$(cat ${deb_chglog} | wc -l)
new_entry_count=$(($new_line_count-$old_line_count))
head -n $new_entry_count $deb_chglog > $tmp_chng_entry
first_ver=$deb_ver
first_run=false

git clean -f
gbp tag --debian-branch=debian
git push

git push --tags
git switch ${mainbranch}
