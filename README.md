# matlab-getdata

## Description
matlab-getdata includes MATLAB functions that simplify retrieving time-series data from LIGO network data servers (NDS).  They abstract away the use of the underlying Java classes of the SWIG binding of the LIGO nds2-client.  They have been tuned with some connection persistence to speed thing when making many repeated queries to the same server.  The files are 

get\_data.m - retrieve time-series data for channels from a local CDS NDS1 server

get\_data2.m - retrieve time-seriies data for channels from a remote LDAS NDS2 server

cdsgetdata.m - retrieve time-series data for channels from appropriate local/remote NDS servers based on channel name and GPS time.

nds2AddJava.m - adds the NDS2 client to the MATLAB Java class path (included in the above)

linux.m - Helper function to run linux commands without MATLAB directory paths

lookback/report\_daq\_framewriter\_lookback\_times.bsh - Shell script to calculate NDS1 frame server lookback times for cdsgetdata

## Installation
These can simply be installed locally and then added to the MATLABPATH.  They do depend on the LIGO nds2-client and its SWIG bindings.  On many existing CDS systems they are linked into /ligo/cds/usermatlab to simplify this

When packaged, the top-level and 'test' routines added to /usr/share/matlab/site/m and then a script added to /etc/profile.d/ to add this to the MATLABPATH

## Usage
There is MATLAB help text for each function, repeated here for clarity

get\_data
```
function [chanListData,warnMsg]=get_data(channels,dtype,start_time,duration)
% GET_DATA - Get data from local NDS1 server on CDS 
%
% chanListData = get_data(channels,dtype,start_time,duration)
%	channels - cell-array of channel names 
%        names are simple format [chName(.trName)]
%         chName - channel name [required]
%         trName - trend name if trend type [optional]
%       dtype: data type (e.g. 'raw','minute','MTREND','second','STREND')
%	start_time - start time in GPS seconds
%	duration - amount of data in seconds
%
% chanListData is a structure with data in it
%   .name - channel name
%   .data - time-series (double)
%   .rate - data rate (Hz)
%   .start - starting time (GPS seconds)
%   .duration - duration of time-series (seconds)
%   .exist - 0 if not found, 1 if found
%   .error - error on this channel
%
% -- to avoid printout of warning messages, add another output parameter
%
%   [chanListData,warnMsg] = get_data(...)
```

get\_data2
```
function [chanListData,warnMsg]=get_data2(channels,dtype,start_time,duration,nds2site)
% GET_DATA2 - Get data from NDS2 server
% 
% chanListData=get_data2(channels,dtype,start_time,duration,nds2site)
%   channels:  cell array of channels 
%   (e.g. {'H1:LSC-DARM_ERR','H2:LSC-DARM_ERR'})
%              or string of just one channel 'H1:LSC-DARM_ERR'
%   dtype: frame('raw','minute','MTREND','second','STREND','L1_DMT_COO',..)
%   start_time: GPS second number (e.g. 855124987)
%   duration:  number of seconds (e.g. 5)
%   nds2site: [OPTIONAL] specify which nds2 server - 'cit','llo' or 'lho'
%
% chanListData is a structures with data in it
% e.g. aa = get_data('H1:LSC-DARM_ERR','raw',871234091,1e5)
%   .name - channel name
%   .data - time-series (double)
%   .rate - data rate (Hz)
%   .start - starting time (GPS seconds)
%   .duration - duration of time-series (seconds)
%   .exist - 0 if not found, 1 if found
%   .error - error on this channel
%
% -- to avoid printout of warning messages, add another output parameter
%   
%   [chanListData,warnMsg] = get_data2(...)
```

cdsgetdata
```
function [chanListData,warnMsg]=cdsgetdata(channels,varargin)
% CDSGETDATA - Get CDS channel data from NDS1/2 server
%
% form 1) chanListData = cdsgetdata(channels,start_time,duration[,nds2site])
%	channels: cell-array of channel names 
%        names are NDS2 format [chName(.trName)(,rate/type),(rate/type)]
%         chName - channel name [required]
%         trName - trend name if trend type [optional]
%         type - frame type (m-trend,s-trend) [optional]
%         rate - data rate (in Hz) [optional]
%	start_time: start time in GPS seconds
%	duration: amount of data in seconds
%   nds2site: [OPTIONAL] specify which nds2 server - 'cit','llo' or 'lho'
%
% form 2) chanListData = cdsgetdata(channels,dtype,start_time,duration[,nds2site])
%	channels - cell-array of channel names 
%        names are simple format [chName(.trName)]
%         chName - channel name [required]
%         trName - trend name if trend type [optional]
%       dtype: data type (e.g. 'raw','minute','MTREND','second','STREND')
%	start_time - start time in GPS seconds
%	duration - amount of data in seconds
%   nds2site: [OPTIONAL] specify which nds2 server - 'cit','llo' or 'lho'
%
% Will use local CDS NDS1 server if data available there, otherwise
%   a remote NDS2 depending on which IFOs (L1, H1, both, etc.) 
%   or what 'nds2site' is set to
%
% chanListData is a structure with data in it
%   .name - channel name
%   .data - time-series (double)
%   .rate - data rate (Hz)
%   .start - starting time (GPS seconds)
%   .duration - duration of time-series (seconds)
%   .exist - 0 if not found, 1 if found
%   .error - error on this channel
%
% -- to avoid printout of warning messages, add another output parameter
%   
%   [chanListData,warnMsg] = cdsgetdata(...)
```
## Testing
Also included are some test routines to verify installation. Installed to /usr/share/matlab/site/m as well.

checkout\_get\_data.m - checks that get\_data is installed and configured to retrieve data from local NDS1 server

checkout\_get\_data2.m - checks that get\_data2 is installed and configured to retrieve data from remote NDS2 server

checkout\_cdsgetdata.m - checks that cdsgetdata is installed and configured to retrieve data both local, remote

get\_data2\_test.m - verify data retrieval from all thre NDS2 servers with get\-data2

cdsgetdata\_test.m - verify data retrieval from local/remote NDS2 servers with cdsgetdata

## Authors and acknowledgment
Keith Thorne, Brian Lantz, Edgard Bonilla

Mnay thanks to LIGO commissioners at LLO for testing this (and breaking it again and again)

## Project status
This is now ready to be installed on production and test stand systems
