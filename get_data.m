function [chanListData,warnMsg]=get_data(channels,dtype,start_time,duration)
% GET_DATA - Get data from local NDS1 server on CDS system
%
% chanListData = get_data(channels,dtype,start_time,duration)
%	channels - cell-array of channel names 
%        names are simple format [chName(.trName)]
%         chName - channel name [required]
%         trName - trend name if trend type [optional]
%   dtype: data type (e.g. 'raw','minute','MTREND','second','STREND')
%	start_time - start time in GPS seconds
%	duration - amount of data in seconds
%
% chanListData is a structure with data in it
%   .name - channel name
%   .data - time-series (double)
%   .rate - data rate (Hz)
%   .start - starting time (GPS seconds)
%   .duration - duration of time-series (seconds)
%   .exist - 0 if not found, 1 if found
%   .error - error on this channel
%
% -- to avoid printout of warning messages, add another output parameter
%   
%   [chanListData,warnMsg] = get_data(...)
%
%   2017-06-01  Keith Thorne - creation
%   2017-10-03  Keith Thorne. change to whole-list fetch, unless error
%   2020-05-08  Keith Thorne - add optional warnMsg output to stop 
%                              warning message preing
%   2021-01-06  Keith Thorne - correct handling of errors for single-channel queries
%   2023-10-06  Edgard Bonilla and Eric Wang - correct name-adding to
%                                              output struct
%   2023-12-14  Keith Thorne - add NDS2_JAR_PATH for alternate nds2.jar
%                            location
%   2024-05-22  Keith Thorne - use external nds2AddJava() in package
%

% clear output
chanListData=[];
warnMsg=[];

% check input count
if (nargin < 4)
   fprintf('GET_DATA ERROR: you only have %d inputs, you need 4\n');
   return 
end

% check output count
warnPrt = true;
if (nargout > 1)
   warnPrt = false;
end

% convert single-channel string to cell-array
if isstr(channels)
  channels = {channels};
end

% Add nds2 Java to java class path
[foundNds2]=nds2AddJava();
if(foundNds2 == false)
    fprintf('GET_DATA ERROR: nds2.jar not in Java classpath\n');
    return
end

% point at the local NDS1 server
localNds=getenv('LIGONDSIP');
localIp=8088;
if(isempty(localNds))
    fprintf('GET_DATA ERROR: LIGONDSIP is not defined\n');
    return
end

% use persistent store to minimize re-connedtions
persistent activeNds;
persistent localConn;

% get nds2-client connection
if ~strcmp(localNds,activeNds)
    activeNds=localNds;
    activeIp=localIp;
    if isempty(localConn)    
        try
            localConn=nds2.connection(activeNds,activeIp);
        catch ME_nds
            fprintf('GET_DATA ERROR: nds connection to %s %d failed %s\n',activeNds,activeIp,ME_nds.message);
            return
        end
      % in CDS, we always patch gaps 
          localConn.setParameter('GAP_HANDLER','STATIC_HANDLER_ZERO');
          fprintf('GET_DATA: nds connection to %s %d succeeded\n',activeNds,activeIp);
    end
end

% truncate the start time to a GPS second
startTime = floor(abs(start_time));
% round duration up to nearest second
durTime = round(duration);
% calculate stop time
stopTime = startTime+durTime;

% If 'raw', add 'raw' type
% If 'second', add 's-trend' add .mean if no such trend definition
% If 'minute', add 'm-trend', add .mean if no such trend definition
%    also if minute trend, set times to divisible by 60
switch dtype
    case 'raw'
% loop over channels. If 'raw' not present, add it.
        dataType = '';    
        defSuffix = []; 
    case 'second'
        dataType = 's-trend';
        defSuffix = 'mean';
    case 'STREND'
        dataType = 's-trend';
        defSuffix = 'mean';
    case 'strend'
        dataType = 's-trend';
        defSuffix = 'mean';
    case 'minute'
        dataType = 'm-trend';
        defSuffix = 'mean';
% For minute trends, it has to be multiple of 60
        startTime = 60*round(startTime/60);
        stopTime = 60*round(stopTime/60);
        durTime = stopTime - startTime;        
    case 'MTREND'
        dataType = 'm-trend';
        defSuffix = 'mean';
% For minute trends, it has to be multiple of 60
        startTime = 60*round(startTime/60);
        stopTime = 60*round(stopTime/60);
        durTime = stopTime - startTime;        
    case 'mtrend'
        dataType = 'm-trend';
        defSuffix = 'mean';
% For minute trends, it has to be multiple of 60
        startTime = 60*round(startTime/60);
        stopTime = 60*round(stopTime/60);
        durTime = stopTime - startTime;        
    case []
        dataType = 'raw';    
        defSuffix = [];         
    otherwise
        fprintf('GET_DATA ERROR: data type of %s not raw, second or minute\n',dtype);
end
% note if changing time range
if ((startTime ~= start_time) || (durTime ~= duration))
    warnMsg = sprintf('WARNING: start time, duration corrected to %d and %d',startTime,durTime);
    if warnPrt
       fprintf('GET_DATA %s\n',warnMsg);
    end    
end

% Loop over channels, adding data type, suffix if needed
[chanRow,chanCol] = size(channels);
%  first we try to do full list in one go.  If there is an error, we 
%  repeat, doing one-channel at a time to get full errors
%  Loop over channels
%    Build up channel list
%  end Loop
%  Do full-list fetch
%  If OKaddpath('/ligo/apps/sl7/nds2-client-0.15.2/share/nds2-client/matlab/+ndsm/');

%     Loop over channels
%        Build output
%     end Loop
%  else (not OK)
%     Loop over channels
%       Build channel desc
%       fetch data for channel normal case - loop over channels to build list, call fetch, 
%       Build up output
%     end loop
%  end
idx=0;
chanGlob=cell(1);
numChan = 0;
for i=1:chanRow
  for j=1:chanCol
      idx = idx + 1; 
      numChan = idx;
      thisChan = channels{i,j};
      [chanName,chanSuf] = strtok(thisChan,'.');
      if (~isempty(defSuffix))
        if (isempty(chanSuf))
          chanSuf = strcat('.',defSuffix);
        end
      end
      if (~isempty(dataType))
        thisChan = strcat(chanName,chanSuf,',',dataType);
      else
        thisChan = strcat(chanName,chanSuf);
      end
% build channel list for 'fetch'        
      chanGlob{1,idx} = thisChan; 
  end
end
% now make the call
try
    dataBuff = localConn.fetch(startTime,stopTime,chanGlob);
    fetchOk = true;
catch ME_fetch
    errMsg = ME_fetch.message;
    fprintf('GET_DATA ERROR: nds2 failed %s\n',ME_fetch.message);
    fetchOk = false; 
end
% If OK, now tediously loop over channels again to pull out data
if (fetchOk ) 
  idx=0;
  for i=1:chanRow
    for j=1:chanCol
        idx=idx+1;
        thisChan = chanGlob{1,idx};
        chanData = dataBuff(idx).getData();
        if ~isempty(chanData)
            chanListData(idx).exist = 1;
            chanListData(idx).error = [];
        else
            chanListData(idx).exist = 0;
            chanListData(idx).error = errMsg;
        end
        chanListData(idx).name = thisChan;
        chanListData(idx).data = double(chanData);
        chanRate = dataBuff(idx).getChannel().getSampleRate();
        chanListData(idx).rate = chanRate;
        chanListData(idx).start = startTime;
        chanListData(idx).duration = durTime;
    end
  end
% if not OK but only one channel we are done
elseif (numChan == 1)
  thisChan = chanGlob{1,1};
  chanListData(1).name = thisChan;
  chanListData(1).data = [];
  chanListData(1).rate = [];
  chanListData(1).start = startTime;
  chanListData(1).duration = durTime;
  chanListData(1).exist = 0;
  chanListData(1).error = errMsg;
% otherwise, re-do per channel to get specific errors
else
  fprintf('GET_DATA: Redo fetch to get per-channel errors\n');   
  idx=0;
  for i=1:chanRow
    for j=1:chanCol
        idx = idx + 1; 
        thisChan = chanGlob{1,idx};
        try
            dataBuff = localConn.fetch(startTime,stopTime,thisChan);
            chanData = dataBuff(1).getData();
            chanRate = dataBuff(1).getChannel().getSampleRate();
        catch ME_chan
            rtIdx = strfind(ME_chan.message,'RuntimeException');
            if(rtIdx > 0)
                nfIdx = strfind(ME_chan.message, 'Requested data were not found');
                if (nfIdx > 0)
                    errMsg = 'Channel not found';
                else
                    errMsg = 'RunTimeException';
                end
            else
                errMsg = ME_chan.message;
            end
            fprintf('GET_DATA ERROR: nds2 failed on %s - %s\n',thisChan,ME_chan.message);  
            chanData = [];
            chanRate = [];
        end   
        chanListData(idx).name = thisChan;
        chanListData(idx).data = double(chanData);
        chanListData(idx).rate = chanRate;
        chanListData(idx).start = startTime;
        chanListData(idx).duration = durTime;
        if ~isempty(chanData)
            chanListData(idx).exist = 1;
            chanListData(idx).error = [];
        else
            chanListData(idx).exist = 0;
            chanListData(idx).error = errMsg;
        end   
    end
  end
end
return
