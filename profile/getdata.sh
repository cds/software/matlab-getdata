#!/bin/sh

prepend()
{
#  prepend - add new item to path if not already there
    newpath=$1
    oldpath=$2
    compath=$3
    if [ -z "${compath}" ]; then
        export ${oldpath}=${newpath}
    else
        if [[ ":$compath:" != *":$newpath:"* ]]; then
            export ${oldpath}=${newpath}:${compath}
        fi
    fi
}
prepend "/usr/share/matlab/site/m" "MATLABPATH" "${MATLABPATH}"
