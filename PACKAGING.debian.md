Debian packaging files are on the 'debian' branch in the 'debian' directory.

Release procedure for Debian

1. On the main branch, tag the desired version with a version number.
2. In the terminal, run ./auto_deb_release.sh <version-number>
   your text editor will pop up, giving you a chance to edit the Debian
   changelog.
3. Edit and save the changelog.
4. Optional. Go to the x1builder jenkins machine and start the package build.
   Otherwise it should start automaticall in 10 minutes.

Package will be added to the debian "unstable" repo.  Ask an LHO CDS person to
promote to testing then to production.
